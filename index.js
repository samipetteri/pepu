console.log('Dir: ' + __dirname);
process.chdir(__dirname);

var requirejs = require("requirejs");
var port = process.env.PORT || 3000;

requirejs.config({
    nodeRequire: require,
    paths: {
        pepu: __dirname + '/pepu'
    }
});

requirejs(["express", "body-parser", "http", "socket.io", "pepu/pepu"],
    function(express, bodyParser, https, ioo, pepu) {
        var app = express();
        var http = https.Server(app);
        var io = ioo(http);

        app.use(express.static("public"));

        app.get('/', function(req, res) {
            res.sendFile(__dirname + '/views/index.html');
        });


        app.get('/osallistu', function(req, res) {
            res.sendFile(__dirname + "/views/lisaa.html");
        });

        var urlencodedParser = bodyParser.urlencoded({ extended: false });
        app.post("/osallistu", urlencodedParser, function(req, res) {
            if (req.body.tarkiste === "mielijuoma") {
                pepu.addUser(req.body.nimi);
                res.sendFile(__dirname + "/views//lisatty.html")
            } else {
                res.sendFile(__dirname + "/views/vaarin.html")
            }
        });

        app.get('/reload', function(req, res) {
            pepu.readUsers();
            res.send("ok");
        });

        http.listen(port, function() {
            console.log('listening on *:%d', port);
        });

        var refreshUsers = function() {
            io.emit('users', { users: pepu.users })
        };

        var refreshUserCount = function() {
            io.emit('viewer count', pepu.listeners);
        };

        pepu.readUsers();

        pepu.onUsersChange(refreshUsers);
        pepu.onViewerCountChange(refreshUserCount);

        io.on('connection', function(socket) {
            pepu.addListener();
            refreshUsers();
            refreshUserCount();

            socket.on('disconnect', function() {
                pepu.removeListener();

            });

            socket.on('start', function(password) {
                if (!password || !password.password || password.password !== "kaljamaha") {
                    socket.emit("wrong");
                    console.log("wrong password!!");
                    return;
                }

                console.log("LOTTERY STARTS");

                refreshUsers();
                pepu.arvo(function(data) {
                    io.emit("remove", data);
                });
            });

            socket.on('list users', function() {
                refreshUsers();
            })
        });
    }
);