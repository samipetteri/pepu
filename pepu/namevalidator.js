define(function(require) {
	return {
		isNameValid: function(name, existingNames) {
			if (name.length < 1)
				return false;

			var found = false;
			for (var i = 0; i < existingNames.length; ++i) {
				if (existingNames[i].toLowerCase() === name.toLowerCase()) {
					found = true;
					break;
				}
			}
			return !found;
		}
	}
});