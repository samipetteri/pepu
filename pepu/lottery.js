define(function(require) {
	return {
		pickNext: function(list, callback) {			
			var index = Math.floor((Math.random() * list.length));
			var toBeRemoved = list[index];	
			list.splice(index, 1);
			callback(list, toBeRemoved);
		}
	}
});