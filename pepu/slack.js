define(function(require) {
	return {
		publishWinner: function(name) {
			var http = require("https");
			var config = require("pepu/config");
			var payload = {
				"text": "Tänä perjantaina janoansa sammuttaa " + name
			};


			if (config.hookPath == "") {
				console.log("no hook, not publishing to slack");
				return;
			}

			var param = JSON.stringify(payload);

			var options = {
				host: "hooks.slack.com",
				path: config.hookPath,
				method: "POST"
			};

			var request = http.request(options, function(res) {
				console.log('STATUS: ' + res.statusCode);
				console.log('HEADERS: ' + JSON.stringify(res.headers));
				res.setEncoding('utf8');
				res.on('data', function (chunk) {
					console.log('BODY: ' + chunk);
				});
				res.on('end', function() {
					console.log('No more data in response.')
				})
			});

			request.write(param);
			request.end();
		}
	}
});
