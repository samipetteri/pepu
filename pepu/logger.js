define(function(require) {
	return {
		dateUtils: require("date-utils"),
		fs: require("fs"),
		logFile: "log",
		log: function(msg) {
			var date = new Date();
			var fullLogMessage = date.toFormat("YYYY-MM-DD HH24:MI:SS") + " " + msg;
			console.log(fullLogMessage);
			this.fs.appendFile(this.logFile, fullLogMessage + "\n");
		}
	}
});