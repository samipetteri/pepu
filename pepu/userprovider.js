define(function(require) {
    return {
        nameValidator: require("pepu/namevalidator"),
        getValidNames: function(names) {
            var users = [];
            for (var i = 0; i < names.length; ++i) {
                names[i] = names[i].trim();

                if (this.nameValidator.isNameValid(names[i], users))
                    users.push(names[i]);
            }
            return users;
        },
        getUserFile: function() {
            var fs = require('fs');
            var dir = "users";
            var filename = dir + "/current.txt";
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
            if (!fs.existsSync(filename))
                fs.closeSync(fs.openSync(filename, 'w'));

            return filename;
        },
        getWeeklyUserFile: function() {
            var fs = require('fs');
            var currentDate = new Date();
            var yearNumber = currentDate.getFullYear();
            var weekNumber = this.getWeekNumber();
            var dir = "users";
            var filename = dir + "/" + yearNumber + "_" + weekNumber + ".txt";
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
            if (!fs.existsSync(filename))
                fs.closeSync(fs.openSync(filename, 'w'));

            return filename;
        },
        readUsersFromFile: function(callback) {
            var fs = require('fs');
            var filename = this.getUserFile();
            var buffer = fs.readFileSync(filename);
            var names = buffer.toString().split("\n");
            var users = this.getValidNames(names);
            callback(users);
        },
        getWeekNumber: function() {
            var curDate = new Date();
            var onejan = new Date(curDate.getFullYear(), 0, 1);
            return Math.ceil((((curDate - onejan) / 86400000) + onejan.getDay() + 1) / 7);
        },
        saveNameToFile: function(user) {
            var fs = require('fs');
            var filename = this.getUserFile();
            fs.appendFile(filename, user + "\n");

            filename = this.getWeeklyUserFile();
            fs.appendFile(filename, user + "\n");
        },
        saveWinner: function(user) {
            var fs = require('fs');
            var filename = this.getUserFile();
            fs.closeSync(fs.openSync(filename, 'w'));
            fs.appendFile(filename, user + "\n");
        }
    }
});