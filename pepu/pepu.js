define(function(require) {
    return {
        userProvider: require("pepu/userprovider"),
        nameValidator: require("pepu/namevalidator"),
        slack: require("pepu/slack"),
        lottery: require("pepu/lottery"),
        logger: require("pepu/logger"),
        listeners: 0,
        users: [],
        addUser: function(user) {
            if (this.nameValidator.isNameValid(user, this.users)) {
                this.userProvider.saveNameToFile(user);
                this.users.push(user);
                this.usersChange();
            }
        },
        readUsers: function() {
            var that = this;
            this.userProvider.readUsersFromFile(function(users) {
                that.users = users;
                that.usersChange();
            });
        },
        running: false,
        arvo: function(callback) {
            var that = this;
            if (this.users.length < 2) {
                if (this.running && this.users.length == 1) {
                    this.logger.log("WINNER: " + this.users[0]);
                    this.slack.publishWinner(this.users[0])
                    this.userProvider.saveWinner(this.users[0]);
                }

                this.running = false;
                return;
            }

            this.running = true;

            setTimeout(function() {
                that.lottery.pickNext(that.users, function(remaining, picked) {
                    that.users = remaining;
                    callback(picked);
                    that.arvo(callback);
                })
            }, 10000);
        },
        addListener: function() {
            this.listeners++;
            this.viewerCountChanged();
        },
        removeListener: function() {
            this.listeners--;
            this.viewerCountChanged();
        },
        userListener: null,
        onUsersChange: function(callback) {
            this.userListener = callback;
        },
        usersChange: function() {
            if (this.userListener != null)
                this.userListener();
        },
        viewerCountListener: null,
        onViewerCountChange: function(callback) {
            this.viewerCountListener = callback;
        },
        viewerCountChanged: function() {
            console.log(this.listeners);
            if (this.viewerCountListener != null)
                this.viewerCountListener()
        }
    }
});